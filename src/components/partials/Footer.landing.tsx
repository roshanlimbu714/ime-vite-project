import { Grid } from "@mantine/core"

export const FooterLanding =()=>{
    return <footer>
        <Grid className="w-full">
            <Grid.Col span={6}>
                <div className="logo font-bold">
                    Logo
                </div>
                <div className="nav-items">
                    <div className="nav-item">Home</div>
                    <div className="nav-item">Features</div>
                    <div className="nav-item">contact</div>
                </div>
            </Grid.Col>
            <Grid.Col span={6}>
                Find us on
            </Grid.Col>
        </Grid>
    </footer>
}