import { Route, Routes } from "react-router"
import { LoginPage } from "../pages/auth/Login.page"
import { SignupPage } from "../pages/auth/Signup.page"
import { Grid } from "@mantine/core"

export const AuthLayout = () => {
    return <>
        <nav className="fixed top-none left-0 w-full">
            <div className="logo">Logo</div>
        </nav>
        <Grid className="auth-grid w-full" style={{height: '100vh'}}>
            <Grid.Col span={6} className="bg-primary-700"></Grid.Col>
            <Grid.Col span={6}>
                <Routes>
                    <Route path='/' element={<LoginPage />} />
                    <Route path='/signup' element={<SignupPage />} />
                </Routes>
            </Grid.Col>
        </Grid>
    </>
}