import { Route, Routes } from "react-router"
import { FooterLanding } from "../components/partials/Footer.landing"
import { NavLanding } from "../components/partials/Nav.landing"
import { FeaturePage } from "../pages/landing/Feature.page"
import { ContactPage } from "../pages/landing/Contact.page"
import { HomePage } from "../pages/landing/Home.page"

export const LandingLayout = () => {
    return <>
        <NavLanding />
        <Routes>
            <Route path="/" element={<HomePage />} />
            <Route path="/features" element={<FeaturePage />} />
            <Route path="/contact" element={<ContactPage />} />
        </Routes>
        <FooterLanding />
    </>
}