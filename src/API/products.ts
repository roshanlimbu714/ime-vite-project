import { GetRequest } from "../plugins/https"


export const APIGetProducts = (params:any)=>{
    return GetRequest('randomproducts', {
        params
    })
}