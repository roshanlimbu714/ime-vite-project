import { Navigate, Route, Routes } from 'react-router';
import { APP_ROUTES } from './routes';
import { IRoute } from './route.interface';
import { AuthLayout } from '../layouts/Auth.layout';
import { LandingLayout } from '../layouts/Landing.layout';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';
import { getToken } from '../utils/helpers/tokenStorage.helper';

export const AppRoutes = () => {

    const isAuthenticated = !!useSelector(
        (state: any) => state.authReducer.accessToken,
    ) || getToken();
    
    useEffect(() => {
       console.log(isAuthenticated);
    }, []);
    return <Routes>
        <Route path={'/auth/*'} element={!isAuthenticated ? <AuthLayout /> : <Navigate to='/' /> }/>
        <Route path={'/*'} element={!isAuthenticated ? <Navigate to='/auth' /> : <LandingLayout />} />
        <Route path={'*'} element={<div>Not found</div>} />
    </Routes>
}