import { AuthLayout } from "../layouts/Auth.layout";
import { LandingLayout } from "../layouts/Landing.layout";
import { HomePage } from "../pages/landing/Home.page";
import { IRoute } from "./route.interface";

export const APP_ROUTES: IRoute[] = [
    {
        path: '/*',
        element: <LandingLayout/>
    },
    {
        path: '/auth/*',
        element: <AuthLayout/>
    }
];