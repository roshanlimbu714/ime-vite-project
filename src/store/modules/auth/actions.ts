import baseAxios from "../../../plugins/axios";
import { getToken } from "../../../utils/helpers/tokenStorage.helper";
import { SET_TOKEN, SET_USER } from "./actionTypes";

export const setUser = (data:any)=>{
    return {
        type: SET_USER,
        payload: data
    }
}

export const setAccessToken = (data:string)=>{
    return {
        type: SET_TOKEN,
        payload: data
    }
}

export const authenticateUser =  (data: {email:string, password:string})=>async (dispatch:any)=>{
    const res= await baseAxios.post('/authentication',{
        username: data.email,
        password: data.password,
    });
    dispatch(setUser(res.data.user[0]));
    dispatch(setAccessToken(res.data.accessToken));
    localStorage.setItem('accessToken', res.data.accessToken);
    localStorage.setItem('user', JSON.stringify(res.data.user[0]));
    setAuthorizationHeader(res?.data.accessToken || '')
}

const setAuthorizationHeader = (token: string) => {
    baseAxios.defaults.headers.common = {
        ...baseAxios.defaults.headers.common,
        Authorization: 'Bearer ' + token,
    }
}

const deleteAuthorizationHeader = () => {
    delete baseAxios.defaults.headers.common.Authorization
}
