import { Button, PasswordInput, TextInput } from "@mantine/core"
import { useForm } from '@mantine/form';
import baseAxios from "../../plugins/axios";
import { useDispatch } from "react-redux";
import { authenticateUser } from "../../store/modules/auth/actions";

export const LoginPage = () => {
  const dispatch: any = useDispatch();
  const form = useForm({
    initialValues: {
      email: '',
      password: '',
    },

    validate: {
      email: (value) => (/^\S+@\S+$/.test(value) ? null : 'Invalid email'),
    },
  });

  const submitForm = async () => {
    await dispatch(authenticateUser({ email: form.values.email, password: form.values.password }))
  }
  return <div className="px-lg flex items-center h-full">
    <form className="w-full" onSubmit={form.onSubmit((values) => submitForm())}>
      <div className="text-3xl mb-xl font-bold">Login</div>
      <div className="">
        <TextInput placeholder="Enter your email" label="Email" {...form.getInputProps('email')} />
        <PasswordInput placeholder="Enter your password" label="Password" mt="sm" required {...form.getInputProps('password')} />
      </div>
      <Button mt="xl" className="w-full" type="submit">Login</Button>
    </form>
  </div>
}