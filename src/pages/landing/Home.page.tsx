import { Button, Grid } from "@mantine/core"
import JoinImg from '../../assets/join.png';
export const HomePage = () => {
    return <>
        <section className="h-screen flex items-center justify-center">
            <div className="text-area text-center">
                <div className="title text-3xl font-bold text-[64px]">Discover your spending habits</div>
                <div className="sub-title  text-[44px] font-bold text-secondary-700 ">Try our expense tracker</div>
                <div className="btn">
                    <Button variant="filled" size="lg">Get Started</Button>
                    <Button variant="outline" size="lg" ml='xs'>Learn More</Button>
                </div>
            </div>
        </section>
        <div className="bg-primary-700 text-white pt-xxl">
        <section
        className="wrapper"
        >
            <Grid>
                <Grid.Col span={4}  p={0}>
                    <div className="pr-md">
                    <div className="text-5xl font-bold">Keep Spending Structured</div>
                    <p className="text-md">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis minus vero dignissimos quam unde, itaque ullam. Totam voluptatibus quisquam iste eveniet, nostrum saepe voluptate error odit odio dolorum architecto ea.</p>
                    </div>
                </Grid.Col>
                <Grid.Col span={8} p={0}>
                    <Grid>
                        {[1, 2, 3, 4].map((_, key) => (
                            <Grid.Col span={6} key={key}>
                                <div className="font-bold text-lg">Title</div>
                                <div className='pt-sm'>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium doloremque aliquid tenetur excepturi nemo ratione sed consequuntur, doloribus ex, accusamus optio animi atque possimus. Alias excepturi impedit eligendi. Laboriosam, corrupti?
                                </div>
                            </Grid.Col>
                        ))

                        }
                    </Grid>
                </Grid.Col>
            </Grid>
        </section>
        <section>
            <Grid>
                <Grid.Col span={5} className="pl-wrapper pt-xxl">
                    <div className="text-3xl">Join 1000_ users in smarter spending</div>
                    
                </Grid.Col>
                <Grid.Col py={0}     span={7} className="flex justify-end items-end">
                    <img src={JoinImg} className="h-[90%] w-[90%] object-cover object-top" alt="" />
                </Grid.Col>
            </Grid>
        </section>
        </div>
    </>
}